// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('epod', ['ionic', 'ngCordova', 'ionic-modal-select', 'epod.controllers'])
.run(function($ionicPlatform, $rootScope, $cordovaSQLite, $ionicLoading, $cordovaLocalNotification) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    db = $cordovaSQLite.openDB({ name : "epod.db" , location : 'default'});
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS simcards (id integer primary key, mobile_number text, status text)");
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS unsent_messages (id integer primary key, mobile_number char(15), message text)");
    // $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS dr_numbers (id integer primary key, sms_id integer, dr_number char(15), job_drop_id char(15), plate_number char(10), so_ref char(15), planned_qty integer, customer_name char(50), customer_mobile char(15), customer_address text, sent_by char(15), date_sent char(15), pin_codes char(25), rejections char(25), confirmed integer, confirmed_date datetime,status char(10))");

    $rootScope.whiteList = ['9171440089'];


    document.addEventListener('deviceready', function () {
      // cordova.plugins.backgroundMode is now available
      cordova.plugins.backgroundMode.setEnabled(true);
      console.log(cordova.plugins.backgroundMode.isActive());
    }, false);

    window.plugins.imei.get(
      function(imei) {
        $rootScope.imei = imei;
      },
      function() {
        console.log("error loading imei");
      }
    );


    $rootScope.showNotification = function (message) {
      var type = message.split("*");
      if (type[0] == 'Q') {
        var notification_message = 'Parking queue has been updated.';
      }
      else if (type[0] == 'DR') {
        var notification_message = 'New DR has been received.';
      }
      else if (type[0] == 'CONF') {
        var notification_message = 'DR ' + type[1] + ' has been completed.';
      }
      else {
        return 0;
      }

      $cordovaLocalNotification.schedule({
        id: 2,
        title: 'Eagle Cement Message',
        text: notification_message,
      }).then(function (result) {
        console.log('Notification 2 triggered');
      });
    }

    $rootScope.insert = function (number, status) {
      var query = "SELECT mobile_number from simcards WHERE mobile_number=?";
      $cordovaSQLite.execute(db, query, [number]).then(function(res) {
          if(res.rows.length == 0) {
              var query = "INSERT INTO simcards(mobile_number, status) VALUES(?, ?)";
              $cordovaSQLite.execute(db, query, [number, status]).then(function (res) {
                console.log(JSON.stringify(res));
              }, function (err) {
                cconsole.log(JSON.stringify(res));
              });
          }
          $scope.hideLoading();
      }, function (err) {
          console.error(err);
          $scope.hideLoading();
      });
    }

    $rootScope.query = function (cmd, params) {
      $cordovaSQLite.execute(db, cmd, params).then(function (res) {
        return res;
      }, function (err) {
        return err;
      });
    }

    document.addEventListener('deviceready', function () {
      // cordova.plugins.backgroundMode is now available
      for (i in $rootScope.whiteList) {
        var check_cmd = "SELECT id FROM simcards WHERE mobile_number=?";
        $cordovaSQLite.execute(db, check_cmd, [$rootScope.whiteList[i]]).then(function(res) {
            if(res.rows.length == 0) {
                $rootScope.insert($rootScope.whiteList[i], 'active');
            }
        }, function (err) {
            console.error(err);
        });
      }

    }, false);

    $rootScope.showLoading = function() {
      $ionicLoading.show({
        template: 'Loading...',
      }).then(function(){
         console.log("The loading indicator is now displayed");
      });
    };
    $rootScope.hideLoading = function(){
      $ionicLoading.hide().then(function(){
         console.log("The loading indicator is now hidden");
      });
    };

    $rootScope.refresh_dr = function() {
        var query = "SELECT * FROM simcards";
        $cordovaSQLite.execute(db, query).then(function(res) {
          if(res.rows.length > 0) {
            for (x = 0; x < res.rows.length; x++) {
              if ($rootScope.whiteList.indexOf(res.rows.item(x).mobile_number) < 0) {
                $rootScope.whiteList.push(res.rows.item(x).mobile_number);
              }
            }
          } else {
            console.log("No results found");
          }

          if(!window.localStorage.getItem("dr_info")) {
            dr_info = {};
            dr_info.dr = {};
            window.localStorage.setItem("dr_info", JSON.stringify(dr_info));
          }

          if(!window.localStorage.getItem("last_sms_id")) {
            window.localStorage.setItem("last_sms_id", 0);
          }

          var last_sms_id = window.localStorage.getItem("last_sms_id");
          var storage = window.localStorage.getItem("dr_info");
          var dr_obj = JSON.parse(storage);
          var message_obj = [];

          if(SMS) SMS.listSMS({maxCount: 1000}, function(data){
            if(Array.isArray(data)) {
              for(var i in data) {
                 var sms = data[i];
                 var ctr = 0;
                 var address = sms.address.slice(-10);
                 if($rootScope.whiteList.indexOf(address) >= 0) {
                  if(sms._id > last_sms_id) {
                    var message_content = sms.body;
                    var m = message_content.split('*');
                    if(m[0] == 'DR' || m[0] == 'CONF' || m[0] == 'NUM') {
                      if (m[0] == 'DR') {
                        if (m[13] == $rootScope.imei) {
                          var obj = {message : message_content, address : sms.address, date_sent : sms.date_sent};
                          message_obj.push(obj);
                        }
                      }
                      else {
                        var obj = {message : message_content, address : sms.address, date_sent : sms.date_sent};
                        message_obj.push(obj);
                      }

                      if(ctr == 0) {
                        window.localStorage.setItem("last_sms_id", sms._id);
                        ctr += 1;
                      }
                    }
                    else if (m.length == 3) {

                    }
                  }
                  else {
                    break;
                  }
                }
                else {
                  var message_content = sms.body;
                  var m = message_content.split('*'); 
                  if(m[0] == 'NUM') {
                    var obj = {message : message_content, address : sms.address, date_sent : sms.date_sent};
                    message_obj.push(obj);

                    if(ctr == 0) {
                      window.localStorage.setItem("last_sms_id", sms._id);
                      ctr += 1;
                    }
                  }
                }
              }

              for(var i in message_obj) {
                message_cont = message_obj[i].message;
                msg = message_obj[i].message;
                var m = msg.split("*");
                if(m[0] == 'DR') {
                  if (!dr_obj.dr[m[1]]) {
                    dr_obj.dr[m[1]] = {};
                    dr_obj.dr[m[1]].sms_id = sms._id;
                    dr_obj.dr[m[1]].dr_number = m[1];
                    dr_obj.dr[m[1]].job_drop_id = m[8];
                    dr_obj.dr[m[1]].plate_number = m[2];
                    dr_obj.dr[m[1]].so_ref = m[3];
                    dr_obj.dr[m[1]].planned_qty = m[7];

                    dr_obj.dr[m[1]].customer = {};
                    dr_obj.dr[m[1]].customer.name = m[4];
                    dr_obj.dr[m[1]].customer.mobile = m[5];
                    dr_obj.dr[m[1]].customer.address = m[6];

                    dr_obj.dr[m[1]].confirmed = 0;
                    dr_obj.dr[m[1]].sent_by = message_obj[i].address;
                    dr_obj.dr[m[1]].date_sent = message_obj[i].date_sent;
                    dr_obj.dr[m[1]].pin_codes = m[9].split(':');
                    dr_obj.dr[m[1]].weigh_in = m[10];
                    dr_obj.dr[m[1]].weigh_out = m[11];
                    dr_obj.dr[m[1]].type = m[12];
                    dr_obj.dr[m[1]].rejections = [];
                  }
                }
                else if (m[0] == 'CONF') {
                  delete dr_obj.dr[m[1]];
                }
                else if (m[0] == 'NUM') {
                  var mobile_numbers = m[1].split(",");
                  for (i in mobile_numbers) {
                    $rootScope.insert(mobile_numbers[i].slice(-10), 'active');
                  }
                }
              }

              window.localStorage.setItem("dr_info", JSON.stringify(dr_obj));
              dr_info = JSON.parse(window.localStorage.getItem("dr_info"));
              var dr_number = [];
              for(var i in dr_info.dr) {
                if (dr_info.dr[i].confirmed == 0) {
                  dr_number.push({'dr' : dr_info.dr[i].dr_number});
                }
              }
              $rootScope.$apply(function() {
                  $rootScope.dr_info = dr_info;
                  $rootScope.dr_numbers = dr_number;
                  $rootScope.actual_delivery_date = Date.now();
              });
            }
          }, function(err){
            updateStatus('error list sms: ' + err);
          });
          //Checking SMS for DR List    
        }, function (err) {
            console.error(err);
        });
        
    }
    $rootScope.refresh_dr($rootScope.dr_numbers);
  });
  
  $rootScope.selected_dr = 'NONE';
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('tabs', {
    url: "/tab",
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
  .state('tabs.driver', {
    url: '/driver',
    views: {
      'driver-tab' : {
        templateUrl: 'templates/driver.html',
        controller: 'DrCtrl'
      }
    }
  })
  .state('tabs.customer', {
    url: '/customer',
    views: {
      'customer-tab' : {
        templateUrl: 'templates/customer.html',
      }
    }
  })
  .state('tabs.queue', {
    url : '/queue',
    views : {
      'queue-tab' : {
        templateUrl : 'templates/queue.html',
        controller : 'QueueCtrl'
      }
    }
  })
  .state('tabs.confirmed', {
    url : '/confirmed',
    views : {
      'confirmed-tab' : {
        templateUrl : 'templates/confirmed.html',
        controller : 'confirmedCtrl'
      }
    }
  })
  .state('tabs.phonebook', {
    url : '/phonebook',
    views : {
      'phonebook-tab' : {
        templateUrl : 'templates/phonebook.html',
        controller : 'phonebookCtrl'
      }
    }
  })
  $urlRouterProvider.otherwise('/tab/driver');
})