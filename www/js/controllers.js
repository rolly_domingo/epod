angular.module('epod.controllers', [])
.controller('DrCtrl', function($scope, $ionicModal, $cordovaToast, $cordovaSms, $rootScope, $cordovaDatePicker, $ionicPlatform, $ionicPopup, $ionicLoading, $cordovaNativeAudio, $cordovaLocalNotification){

  document.addEventListener("deviceready", function () {
    if(SMS) SMS.startWatch(function(){
      $scope.refresh_dr();
    }, function(){
      updateStatus('failed to start watching');
    });
  });


  $scope.unload_obj = {
    unload_pin : '',
    unloading : false
  };

  $scope.send_sms = function(t) {
    var sent_date = new Date();
    if (t == 1) {
      var rejections_arr = [];
      for (i in $scope.dr.rejections) {
        rejections_arr.push($scope.dr.rejections[i].code + "-" + $scope.dr.rejections[i].quantity);
      }

      var remarks = $scope.dr.dr_remarks ? $scope.dr.dr_remarks : '';
      var rejections = rejections_arr.join(",");
      var actual_qty = parseInt($scope.dr.planned_qty) - parseInt($scope.calculateRejects());
      var sms_message = $scope.dr.dr_number;
          sms_message += ":" + parseInt($scope.dr.planned_qty);
          sms_message += ":" + actual_qty;
          sms_message += ":" + rejections; 
          sms_message += ":" + $scope.contact_pin;
          sms_message += ":" + sent_date.getTime() / 1000;
          sms_message += ":" + remarks;
          sms_message += ":" + $scope.dr.job_drop_id;  
    }
    else {
      var remarks = $scope.dr.dr_remarks ? $scope.dr.dr_remarks : '';
      var sms_message = $scope.dr.dr_number + ":" + parseInt($scope.dr.planned_qty);
          sms_message += ":0:cancelled-0:" + $scope.contact_pin;
          sms_message += ":" + sent_date.getTime() / 1000;
          sms_message += ":" + remarks;
          sms_message += ":" + $scope.dr.job_drop_id;
    }
    
    var options = {
      replaceLineBreaks: false, // true to replace \n by a new line, false by default
      android: {
          intent: ''  // send SMS with the native android SMS messaging
      }
    };

    for (i in $rootScope.whiteList) {
      var number = '+63' + $rootScope.whiteList[i];
      $cordovaSms.send(number, sms_message, options)
      .then(function(){

      }, function(error) {  
        var cmd = "INSERT INTO unsent_messages(mobile_number, message) VALUES(?, ?)";
        $scope.query(cmd, [number, sms_message]);
        $scope.showToast("Insufficient load balance or No network connection.", "long", "center");
      });
    }

    $scope.selected_dr = 'NONE';
    $scope.contact_name = '';
    update_storage($scope.dr.dr_number);
    if (t == 1) {
      $scope.showToast("DR confirmation has been sent", "long", "center");
    }
    else {
      $scope.showToast("DR has been cancelled", "long", "center");
    }
    $scope.dr = {};
  }

  $scope.reject = {};
  
  $scope.showLoading = function() {
    $ionicLoading.show({
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

  $scope.hideLoading = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };

  $scope.showAlert = function(title, message) {
   var alertPopup = $ionicPopup.alert({
     title: title,
     template: message
   });
  };

  $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope,
    controller: 'DrCtrl',
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function() {
      $scope.modal.show();
   };

   $scope.closeModal = function() {
      $scope.contact_name = "";
      $scope.modal.hide();
   };

   $scope.select_dr = function(drvalue) {
      $scope.unload_obj = {
        unload_pin : '',
        unloading : false
      };

      $scope.selected_dr = drvalue;
      $scope.contact_name = "";
      $scope.dr = $rootScope.dr_info.dr[drvalue];
      $scope.modal.hide();
   }

   $ionicModal.fromTemplateUrl('templates/modal-pin.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal_pin = modal;
    });

    $ionicModal.fromTemplateUrl('templates/modal-rejection.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.rejection_modal = modal;
    });

    $scope.confirm = {};

    $scope.openPinModel = function(type) {
      $scope.confirm.pin = '';
      $scope.confirm.type = type;
      $ionicPopup.show({
        title : 'PIN Code',
        template : '<input type="text" placeholder="Enter PIN Code" class="input-large text-center" ng-model="confirm.pin">',
        scope : $scope,
        buttons : [
          {text : 'Close'},
          {
            text : 'Submit', 
            type : 'button-positive',
            onTap : function (e) {
              if (!$scope.confirm.pin) {
                e.preventDefault();
              }
              else {
                var pincrypt = ['C', 'A', 'L', 'I', 'B', 'R', '8', 'S', 'Y', 'Z', 'D', 'Q', 'P', 'X', 'O', '4', 'E', 'W', 'K', '1', 'J', '3', 'T', 'F', 'N', 'U', 'H', 'V', 'G', 'M'];
                var fret = ['5', '8', '1', '9', '2', '7', '4', '0', '6', '3', 'A', 'B', 'C',  'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'];
                var pin_arr = $scope.confirm.pin.split("");
                var pindecrypt = "";
                var returning_pin = [];
                for(x = 0; x < pin_arr.length; x++) {
                  var i = $scope.getPinIndex(pin_arr[x]);
                  if (i >= 0) {
                    pindecrypt += pincrypt[i].toString();
                    returning_pin.push(i);
                  }
                }
                
                if($scope.dr.pin_codes.indexOf(pindecrypt) >= 0) {
                  if ($scope.confirm.type == 'accept') {
                    var confirm = $ionicPopup.confirm({
                      template : 'Are you sure you want to submit?',
                      title : 'Sumbit Confirmation',
                      okType : 'button-positive',
                      cancelType : 'button-stable'
                    });
                    confirm.then(function (res) {
                      if (res) {
                        $scope.contact_pin = returning_pin.join(".");
                        $scope.dr.dr_confirmed_by = $scope.dr.customer.name;
                        $scope.showToast("Thank you " + $scope.dr.customer.name + '\n Please wait while we are processing your request.', "long", "center");
                        $scope.send_sms(1);
                        $scope.modal_pin.hide();
                      }
                    });
                  }
                  else {
                    var confirm = $ionicPopup.confirm({
                      template : 'Are you sure you want to cancel this delivery?',
                      title : 'Sumbit Cancellation',
                      okType : 'button-positive',
                      cancelType : 'button-stable'
                    });
                    confirm.then(function (res) {
                      if (res) {
                        $scope.contact_pin = returning_pin;
                        $scope.dr.dr_confirmed_by = $scope.dr.customer.name;
                        $scope.showToast('Please wait while we are processing your request.', "long", "center");
                        $scope.send_sms(0);
                        $scope.modal_pin.hide();
                      }
                    });
                  }
                }
                else {
                  $scope.showToast("Incorrect PIN Code", "long", "bottom");
                  e.preventDefault();
                }
              }
            }
          }
        ]
      })
    }

    $scope.closePinModel = function() {
      $scope.modal_pin.hide();
    }

    $scope.getPinIndex = function (i) {
      var array = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
      return array.indexOf(i);
    }

    $scope.decryptedPin = function (pin) {
      var pincrypt = ['C', 'A', 'L', 'I', 'B', 'R', '8', 'S', 'Y', 'Z', 'D', 'Q', 'P', 'X', 'O', '4', 'E', 'W', 'K', '1', 'J', '3', 'T', 'F', 'N', 'U', 'H', 'V', 'G', 'M'];
      var fret = ['5', '8', '1', '9', '2', '7', '4', '0', '6', '3', 'A', 'B', 'C',  'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'];
      var pin_arr = pin.split("");
      var pindecrypt = "";
      for(x = 0; x < pin_arr.length; x++) {
        var i = $scope.getPinIndex(pin_arr[x]);
        if (i >= 0) {
          pindecrypt += pincrypt[i].toString(); 
        }
      }
      return pindecrypt;
    }

    $scope.pin_entered = function() {
      var pincrypt = ['C', 'A', 'L', 'I', 'B', 'R', '8', 'S', 'Y', 'Z', 'D', 'Q', 'P', 'X', 'O', '4', 'E', 'W', 'K', '1', 'J', '3', 'T', 'F', 'N', 'U', 'H', 'V', 'G', 'M'];
      var fret = ['5', '8', '1', '9', '2', '7', '4', '0', '6', '3', 'A', 'B', 'C',  'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'];
      var pin_arr = $scope.confirm.pin.split("");
      var pindecrypt = "";
      var returning_pin = [];
      for(x = 0; x < pin_arr.length; x++) {
        var i = $scope.getPinIndex(pin_arr[x]);
        if (i >= 0) {
          pindecrypt += pincrypt[i].toString();
          returning_pin.push(i);
        }
      }

      if($scope.dr.pin_codes.indexOf(pindecrypt) >= 0) {
        if ($scope.confirm.type == 'accept') {
          var confirm = $ionicPopup.confirm({
            template : 'Are you sure you want to submit?',
            title : 'Sumbit Confirmation',
            okType : 'button-positive',
            cancelType : 'button-stable'
          });
          confirm.then(function (res) {
            if (res) {
              $scope.contact_pin = returning_pin.join(".");
              $scope.dr.dr_confirmed_by = $scope.dr.customer.name;
              $scope.showToast("Thank you " + $scope.dr.customer.name + '\n Please wait while we are processing your request.', "long", "center");
              $scope.send_sms(1);
              $scope.modal_pin.hide();
            }
          });
        }
        else {
          var confirm = $ionicPopup.confirm({
            template : 'Are you sure you want to cancel this delivery?',
            title : 'Sumbit Cancellation',
            okType : 'button-positive',
            cancelType : 'button-stable'
          });
          confirm.then(function (res) {
            if (res) {
              $scope.contact_pin = returning_pin;
              $scope.dr.dr_confirmed_by = $scope.dr.customer.name;
              $scope.showToast('Please wait while we are processing your request.', "long", "center");
              $scope.send_sms(0);
              $scope.modal_pin.hide();
            }
          });
        }

        return true;
      }
      else {
        return false;
      }
    }

    $scope.showToast = function(message, duration, location) {
        $cordovaToast.show(message, duration, location).then(function(success) {
            console.log("The toast was shown");
        }, function (error) {
            console.log("The toast was not shown due to " + error);
        });
    }

    $scope.rejection_codes = [
      { code: "1", description : "Diversion"},
      { code: "2", description : "Cement Hardened"},
      { code: "3", description : "Under-weight"},
      { code: "4", description : "Wrong SKU/Product"},
    ];

    $scope.reject = {quantity : 0};
    $scope.total_rejections = 0;
    
    $scope.rejected_codes = [];
    $scope.set_rejection_code = function(robj) {
      var popup = $ionicPopup.show({
        template : '<input type="number" placeholder="Enter Rejected Quantity" ng-model="reject.quantity">',
        title : 'Rejected Quantity',
        scope : $scope,
        buttons : [
          {text : 'Close'},
          {
            text : 'Save',
            type : 'button-positive',
            onTap : function (e) {
              if (!$scope.reject.quantity || $scope.reject.quantity < 1) {
                e.preventDefault();
              }
              else {
                var rejection_obj = {code : robj.code, description : robj.description, quantity : $scope.reject.quantity};
                if ($scope.rejected_codes.indexOf(robj.code) >= 0) {
                  var idx = $scope.rejected_codes.indexOf(robj.code);
                  $scope.dr.rejections[idx] = rejection_obj; 
                }
                else {
                  $scope.rejected_codes.push(robj.code);
                  $scope.dr.rejections.push(rejection_obj);
                }

                $scope.reject.quantity = 0;
                $scope.rejection_modal.hide();
              }
            }
          }
        ]
      });
    }

    $scope.removeRejection = function (obj) {
      var confirm = $ionicPopup.confirm({
        title : 'Remove rejection',
        template : 'You\'re about to remove the rejection details?',
        okType : 'button-assertive',
        cancelType : 'button-stable',
        okText : 'Ok',
        cancelText : 'Cancel'
      });

      confirm.then(function (res) {
        if (res) {
          var idx = $scope.dr.rejections.indexOf(obj);
          $scope.dr.rejections.splice(idx, 1);
          $scope.rejected_codes.splice(idx, 1);
        }
      })
    }

    $scope.calculateRejects = function () {
      var total = 0;
      if ($scope.dr) {
        for (i in $scope.dr.rejections) {
          total += $scope.dr.rejections[i].quantity;
        }
      }
      return total;
    }

    $scope.unload = function () {
      $ionicPopup.show({
        title : 'PIN Code',
        scope : $scope,
        template : '<input type="text" placeholder="Enter PIN code" ng-model="unload_obj.unload_pin" class="input-large text-center">',
        buttons : [
          {text : 'Cancel'},
          {
            text : 'Ok',
            type : 'button-positive',
            onTap : function (e) {
              if (!$scope.unload_obj.unload_pin) {
                e.preventDefault();
              }
              else {
                var decrypted_pin = $scope.decryptedPin($scope.unload_obj.unload_pin);
                if ($scope.dr.pin_codes.indexOf(decrypted_pin) >= 0) {
                  $scope.unload_obj = {
                    unload_pin : '',
                    unloading : true,
                  };
                }
                else {
                  $scope.showToast("Incorrect PIN code", "long", "bottom");
                  e.preventDefault();
                }
              }
            }
          }
        ]
      })
    }

    document.addEventListener('onSMSArrive', function(e){
      var sms = e.data;
      var mobile = sms.address.slice(-10);
      if ($rootScope.whiteList.indexOf(mobile) >= 0) {
        var sms_arr = sms.body.split('*');
        if (sms_arr[0] == 'DR') {
          if (sms_arr[13] == $rootScope.imei) {
            $rootScope.showNotification(sms.body);
            setTimeout(function (){
              $rootScope.refresh_dr();
            }, 1500);
          }
        }
        else {
          setTimeout(function (){
            $rootScope.refresh_dr();
            $rootScope.showNotification(sms.body);
          }, 1500);
        }
        
      }
    });

  var update_storage = function(drn) {
    $rootScope.dr_info.dr[drn].confirmed = 1;
    var index = getDrIndex(drn);
    $rootScope.dr_numbers.splice(index, 1);
    window.localStorage.setItem('dr_info', JSON.stringify($rootScope.dr_info));
  }

  var getDrIndex = function (dr) {
    for (i in $rootScope.dr_numbers) {
      if($rootScope.dr_numbers[i].dr == dr) {
        return i;
      }
    }
  }
})
.controller('DrListCtrl', function($scope, $rootScope) {
  // alert("Root Scope : " + $rootScope.dr_info);
  // var dr_numbers = [];
  // for(dr_number in $rootScope.dr_info.dr) {
  //   dr_numbers.push({'dr' : dr_number});
  // }
  // console.log(JSON.stringify(dr_numbers));
  
})
.controller('PinCtrl', ['$scope', 
  function($scope) {
    $scope.pin_entered = function() {
      $scope.$emit('pin_updated', {
        pin : $scope.pin,
      });
    }
  }
])
.controller('QueueCtrl', ['$scope', '$cordovaToast', '$rootScope', '$state',
  function($scope, $cordovaToast, $rootScope, $state) {
    $scope.showToast = function(message, duration, location) {
      $cordovaToast.show(message, duration, location).then(function(success) {
          console.log("The toast was shown");
      }, function (error) {
          console.log("The toast was not shown due to " + error);
      });
    }

    $scope.truck = {};
    
    $scope.truck.plate_number = window.localStorage.getItem("plate");
    $scope.queue = {};

    $scope.save_plate = function () {
      $scope.showToast("Your plate number has been saved", "long", "center");
      window.localStorage.setItem("plate", $scope.truck.plate_number)
    }


    $scope.refresh_list = function () {
      var last_sms = window.localStorage.getItem("last_sms_id");
      $scope.queue = JSON.parse(window.localStorage.getItem("queue"));
      if(SMS) SMS.listSMS({maxCount: 10000}, function(data){
        if(Array.isArray(data)) {
          for(var i in data) {
             var sms = data[i];
             var address = sms.address.slice(-10);
             if($scope.whiteList.indexOf(address) >= 0) {
                if(sms._id > last_sms) {
                  var plates_arr_bag = [];
                  var plates_arr_bulk = [];
                  var proceed_arr= [];
                  m_content = sms.body.split("*");
                  if (m_content[0] == 'Q') {
                    m_header = m_content[1].split(':');
                    $scope.queue_priority = m_header[0] == 1 ? 'Express' : 'Regular';
                    $scope.queue_type = m_header[1] == 1 ? 'Bulk' : 'Bag';
                    $scope.proceed_to = m_content[4];
                    m = m_content[2].split(':');
                    for (mi in m) {
                      var type = m[mi].slice(-1);
                      var plate_no = m[mi].slice(0, -1);

                      if (type == 1) {
                        proceed_arr.push({plate_number : plate_no, priority : $scope.queue_priority});
                      }
                      else {
                        if (m_header[1] == 1) {
                          plates_arr_bulk.push({plate_number : plate_no, priority : $scope.queue_priority});  
                        }
                        else {
                          plates_arr_bag.push({plate_number : plate_no, priority : $scope.queue_priority});  
                        }
                        
                      }
                    }

                    $scope.queue = {};
                    $scope.queue.bags = plates_arr_bag;
                    $scope.queue.bulk = plates_arr_bulk;
                    $scope.queue.proceed = proceed_arr;
                    $scope.truck.plate_number = m_content[3];
                    $scope.$digest();

                    window.localStorage.setItem("last_sms_id", sms._id);
                    window.localStorage.setItem("queue", JSON.stringify($scope.queue));
                    window.localStorage.setItem("plate", m_content[3]);
                  }
                }
              }
              else {
                break;
              }
            }
          }
      }, function(err){
        alert(JSON.stringify(err));
      });
    }


    setTimeout(function () {
      $scope.refresh_list();
      $scope.$digest();
    }, 2000);

    document.addEventListener('onSMSArrive', function(e){
      var sms = e.data;
      var mobile = sms.address.slice(-10);
       if ($rootScope.whiteList.indexOf(mobile) >= 0) {
        var sms_arr = sms.body.split('*');
        if (sms_arr[0] == 'DR') {
          if (sms_arr[13] == $rootScope.imei) {
            $rootScope.showNotification(sms.body);
            setTimeout(function (){
              $scope.queue = {};
              window.localStorage.setItem("queue", JSON.stringify($scope.queue));
              $rootScope.refresh_dr();
            }, 1500);
            $state.go('tabs.driver');
          }
        }
        else {
          setTimeout(function (){
            $scope.refresh_list();
            $rootScope.refresh_dr();
            $rootScope.showNotification(sms.body);
          }, 1500);
        }
      }
    });
  }
])
.controller('confirmedCtrl', function($scope, $rootScope) {

})
.controller('phonebookCtrl', function($scope, $rootScope, $cordovaSQLite, $ionicModal) {
  $ionicModal.fromTemplateUrl('templates/phonebook-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.admin = {};

  $scope.viewPhonebook = function () {

    if ($scope.admin.password == 675873763) {
      $scope.showLoading();
      $scope.numbers = [];
      setTimeout(function () {
        document.addEventListener("deviceready", function () {
          var query = "SELECT mobile_number from simcards";
            $cordovaSQLite.execute(db, query, []).then(function(res) {
                if(res.rows.length > 0) {
                    for (i = 0; i < res.rows.length; i++) {
                      $scope.numbers.push({number : res.rows.item(i).mobile_number});
                    }
                    $scope.modal.show();
                } else {
                    console.log("No results found");
                }
                $scope.hideLoading();
            }, function (err) {
                console.error(err);
                $scope.hideLoading();
            });
        });
      }, 2000);
    }
    else {
      alert("Invalid Password");
    }
  }

  $scope.reload = function () {
    $scope.showLoading();
    $scope.numbers = [];
    setTimeout(function () {
      document.addEventListener("deviceready", function () {
        var query = "SELECT mobile_number from simcards";
          $cordovaSQLite.execute(db, query, []).then(function(res) {
              if(res.rows.length > 0) {
                  for (i = 0; i < res.rows.length; i++) {
                    $scope.numbers.push({number : res.rows.item(i).mobile_number});
                  }
              } else {
                  console.log("No results found");
              }
              $scope.hideLoading();
          }, function (err) {
              console.error(err);
              $scope.hideLoading();
          });
      });
    }, 1000);
  }

  $scope.hideModal = function () {
    $scope.admin = {};
    $scope.modal.hide();
  }

  $scope.deleteAll = function () {
    var delete_cmd = "DELETE FROM simcards";
    $scope.query(delete_cmd, []);
    $scope.reload();
  }
});